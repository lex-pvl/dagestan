$(document).ready(function() {
	$('.form').each(function(){
		$(this).validate({
			errorClass: 'bad',
			rules: {
				name: {
					required: true
				},
				phone: {
					required: true,
				},
				mail: {
					required: true,
					email: true
				},
				password: {
					required: true
				},
				repassword: {
					required: true,
					equalTo: '#password'
				}
			},
			messages: {
				name: {
					required: 'Введите данные'
				},
				phone: {
					required: 'Номер введен неправильно*'
				},
				mail: {
					required: 'Поле обязательно для заполнения',
					email: 'Введите корректный адрес'
				},
				password: {
					required: 'Поле обязательно для заполнения'
				},
				repassword: {
					required: 'Поле обязательно для заполнения',
					equalTo: 'Пароли не совпадают'
				}
			}
		})
	});


	$('#order').validate({
		errorClass: 'bad',
		rules: {
			name: {
				required: true
			},
			phone: {
				required: true,
			},
			region: {
				required: true
			},
			city: {
				required: true
			},
			street: {
				required: true
			},
			home: {
				required: true
			}
		},
		messages: {
			name: {
				required: 'Введите данные'
			},
			phone: {
				required: 'Номер введен неправильно*'
			},
			region: {
				required: 'Поле обязательно к заполнению'
			},
			city: {
				required: 'Поле обязательно к заполнению'
			},
			street: {
				required: 'Поле обязательно к заполнению'
			},
			home: {
				required: 'Поле обязательно к заполнению'
			}
		}
	});
});
