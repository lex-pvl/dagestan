$(document).ready(function() {
	sliders();
	cardValue();
	up();
	filtersShow();
	accordeon();
	changeImages();
	tabs();
	showTextInProduct();
	showPopup();
	openCatalog();
	copyLink();
	showPassword();
	orderLabels();
	selects();
	initMap();
	brandFilters();
	fixedHeader();
	hideFixedHeaderLink();
	replaceSymbols();
	phoneMask();
	mediaQuery();
	autoHeight();
});


function sliders() {
	var mainSlider = new Swiper('.main-container', {
		lazy: true,
		slidesPerView: 1,
		spaceBetween: 20,
		loop: true,
		autoplay: {
			delay: 5000
		},
		navigation: {
			nextEl: '.main-btn-next',
			prevEl: '.main-btn-prev',
		},
		pagination: {
			el: '.main-pagination',
			clickable: true,
		},
	});

	var newSlider = new Swiper('.new-container', {
		lazy: true,
		allowTouchMove: false,
		navigation: {
			nextEl: '.new-btn-next',
			prevEl: '.new-btn-prev',
		},
		breakpoints: {
			320: {
				slidesPerView: 1,
				spaceBetween: 20,
				allowTouchMove: true,
			},
			801: {
				slidesPerView: 3,
				spaceBetween: 20,
			},
			1025: {
				slidesPerView: 4,
				spaceBetween: 20,
			},
			1366: {
				slidesPerView: 5,
				spaceBetween: 20,
			}
		}
	});

	var lastSlider = new Swiper('.last-container', {
		lazy: true,
		allowTouchMove: false,
		navigation: {
			nextEl: '.last-btn-next',
			prevEl: '.last-btn-prev',
		},
		breakpoints: {
			320: {
				slidesPerView: 1,
				spaceBetween: 20,
				allowTouchMove: true,
			},
			801: {
				slidesPerView: 3,
				spaceBetween: 20,
			},
			1025: {
				slidesPerView: 4,
				spaceBetween: 20,
			},
			1366: {
				slidesPerView: 5,
				spaceBetween: 20,
			}
		}
	});

	var newsSlider = new Swiper('.news-container', {
		lazy: true,
		allowTouchMove: false,
		navigation: {
			nextEl: '.news-btn-next',
			prevEl: '.news-btn-prev',
		},
		breakpoints: {
			320: {
				slidesPerView: 1,
				spaceBetween: 23,
				allowTouchMove: true,
			},
			800: {
				slidesPerView: 3,
				spaceBetween: 23,
			},
			1024: {
				slidesPerView: 4,
				spaceBetween: 23,
			},
			1366: {
				slidesPerView: 4,
				spaceBetween: 23,
			}
		}
	});

	var partnersSlider = new Swiper('.partners-container', {
		lazy: true,
		slidesPerView: 6,
		spaceBetween: 64,
		centeredSlides: true,
		loop: true,
		autoplay: {
			delay: 2000
		},
		breakpoints: {
			320: {
				slidesPerView: 2,
				spaceBetween: 23,
			},
			800: {
				slidesPerView: 4,
				spaceBetween: 23,
			},
			1024: {
				slidesPerView: 6,
				spaceBetween: 64,
			},
			1366: {
				slidesPerView: 6,
				spaceBetween: 64,
			}
		}
	});

	var projectThumbs = new Swiper('.project-thumbs', {
		freeMode: false,
		watchSlidesVisibility: true,
		watchSlidesProgress: true,
		breakpoints: {
			320: {
				spaceBetween: 12,
				slidesPerView: 2,
			},
			801: {
				spaceBetween: 12,
				slidesPerView: 5,
			}
		}
	});
	var galleryTop = new Swiper('.project-top', {
		spaceBetween: 10,
		thumbs: {
			swiper: projectThumbs
		}
	});

	var sertSlider = new Swiper('.sert-container', {
		navigation: {
			nextEl: '.sert-btn-next',
			prevEl: '.sert-btn-prev',
		},
		breakpoints: {
			320: {
				slidesPerView: 1,
				spaceBetween: 30,
			},
			800: {
				slidesPerView: 3,
				spaceBetween: 30,
			},
			1024: {
				slidesPerView: 4,
				spaceBetween: 30,
			}
		}
	})
}

function cardValue() {
	var minus = $('.minus'),
	plus = $('.plus'),
	input = $('.value');

	input.each(function() {
		let max = +$(this).attr('max');

		$(this).on('change input', function() {
			if ($(this).val() <= 1) {
				$(this).siblings('.minus').addClass('disabled');
				$(this).siblings('.minus').prop('disabled', true);
			} else {
				$(this).siblings('.minus').removeClass('disabled');
				$(this).siblings('.minus').prop('disabled', false);
			}

			if ($(this).val() >= max) {
				$(this).siblings('.plus').addClass('disabled');
				$(this).siblings('.plus').prop('disabled', true);
				$(this).val(max);
			} else {
				$(this).siblings('.plus').removeClass('disabled');
				$(this).siblings('.plus').prop('disabled', false);
			}
		});

		if ($(this).val() <= 1) {
			$(this).siblings('.minus').addClass('disabled');
			$(this).siblings('.minus').prop('disabled', true);
		} else {
			$(this).siblings('.minus').removeClass('disabled');
			$(this).siblings('.minus').prop('disabled', false);
		}

		if ($(this).val() >= max) {
			$(this).siblings('.plus').addClass('disabled');
			$(this).siblings('.plus').prop('disabled', true);
			$(this).val(max);
		} else {
			$(this).siblings('.plus').removeClass('disabled');
			$(this).siblings('.plus').prop('disabled', false);
		}
	});

	minus.each(function() {
		$(this).on('click', function(event) {
			event.preventDefault();
			var max = +$(this).siblings('.value').attr('max');
			var qty = +$(this).siblings('.value').val();
			qty = qty - 1;
			$(this).siblings('.value').val(qty);

			if (qty <= 1) {
				$(this).addClass('disabled');
				$(this).prop('disabled', true);
			} else {
				$(this).removeClass('disabled');
				$(this).prop('disabled', false);
			}

			if (qty >= max) {
				$(this).siblings('.plus').addClass('disabled');
				$(this).siblings('.plus').prop('disabled', true);
			} else {
				$(this).siblings('.plus').removeClass('disabled');
				$(this).siblings('.plus').prop('disabled', false);
			}
		});
	});

	plus.each(function() {
		$(this).on('click', function(event) {
			event.preventDefault();
			var max = +$(this).siblings('.value').attr('max');
			var qty = +$(this).siblings('.value').val();
			qty = qty + 1;
			$(this).siblings('.value').val(qty);

			if (qty > 1) {
				$(this).siblings('.minus').removeClass('disabled');
				$(this).siblings('.minus').prop('disabled', false);
			} else {
				$(this).siblings('.minus').addClass('disabled');
				$(this).siblings('.minus').prop('disabled', true);
			}

			if (qty >= max) {
				$(this).addClass('disabled');
				$(this).prop('disabled', true);
			} else {
				$(this).removeClass('disabled');
				$(this).prop('disabled', false);
			}
		});
	});
}

function up() {
	$('#up').on('click', function(e){
		e.preventDefault();
		$('html,body').stop().animate({ scrollTop: 0 }, 1000);
	});
}

function filtersShow() {
	var list = $('.catalog-list');

	list.each(function() {
		var link = $(this).find('.catalog-link');
		link.on('click', function(event) {
			event.preventDefault();
			$(this).next().slideToggle(400);
		});
	});
}

function initMap() {
	if ($('#map').is('#map')) {
		ymaps.ready(init);
		var iconUrl = $('#map').data('icon-url');
		var lat = $('#map').data('lat');
		var lng = $('#map').data('lng');
		var zoom = $('#map').data('zoom');

		function init() {
			var map = new ymaps.Map('map', {
				center: [lng, lat],
				zoom: zoom,
				controls: [],
				behaviors: ['drag']
			});

			var placemark = new ymaps.Placemark([lng, lat], {
				hintContent: ''
			},
			{
				iconLayout: 'default#image',
				iconImageHref: iconUrl,
				iconImageSize: [36, 52]
			});

			console.log(placemark)

			map.geoObjects.add(placemark)
		}
	}
}


function accordeon() {
	var head = $('.faq__head'),
	body = $('.faq__body');

	head.each(function() {
		$(this).on('click', function() {
			$(this).toggleClass('active');
			$(this).next().slideToggle();
		});
	});
}

function changeImages() {
	var iframe = $('#video-item');
	var img = $('#img-item');
	var item = $('.left-item');

	item.each(function() {
		$(this).on('click', function() {
			$(this).siblings().removeClass('active');
			$(this).addClass('active');
			if ($(this).is('img') && !$(this).is('.tovar-video')) {
				iframe.hide();
				img.show();
				img.attr('src', $(this).attr('src'));
			}
			else if ($(this).is('.tovar-video')) {
				img.hide();
				iframe.show();
				iframe.attr('src', $(this).attr('data-src'));
			}
		});
	});
}

function tabs() {
	$('ul.tovar__list').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('.tovar').find('div.tovar__tab').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('ul.popup__list').each(function() {
		$(this).on('click', 'li:not(.active)', function() {
			$(this)
			.addClass('active').siblings().removeClass('active')
			.closest('.popup__tabs').find('div.popup__tab').removeClass('active').eq($(this).index()).addClass('active');
		});
	});

	// $('ul.lk__list').on('click', 'li:not(.active)', function() {
	// 	$(this)
	// 	.addClass('active').siblings().removeClass('active')
	// 	.closest('.lk').find('div.lk__tab').removeClass('active').eq($(this).index()).addClass('active');
	// });

	$('ul.delivery__list').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('.delivery').find('div.delivery__tab').removeClass('active').eq($(this).index()).addClass('active');
	});

}

function showTextInProduct() {
	var limit = 3;
	var text = $('.show-text').text();
	$(".tovar__text p:nth-child(n + " + (limit + 1) + ")").hide();

	$(".show-text").click(function(e) {
		e.preventDefault();

		if ($(".tovar__text p:eq(" + limit + ")").is(":hidden")) {
			$('.blur').hide();
			$(".tovar__text p:hidden").slideDown(400);
			$('.show-text').text('Свернуть описание');
		} else {
			$(".tovar__text p:nth-child(n + " + (limit + 1) + ")").slideUp(400);
			$('.blur').show();
			$('.show-text').text(text);
		}
	});
}

function showPopup() {
	var popup = $('.popup');
	var overlay = $('.overlay');
	var width = $(window).width();
	var close = $('.popup__close');
	var popupOpen;
	var popupClose;

	popupOpen = function(popupName){
		$('body').css('width', width);
		$('body').addClass('no-scroll');
		popup.removeClass('active');
		$('.'+popupName).addClass('active');
		overlay.show();
	}
	popupClose = function(){
		popup.removeClass('active');
		overlay.hide();
		$('body').removeClass('no-scroll');
	}
	close.on('mousedown', function(event){
		event.preventDefault();
		popupClose();
	});
	overlay.on('mousedown', function(){
		popupClose();
	});
	$('body').on('click','[data-popup-open]',function(e){
		e.preventDefault();
		var popupName = $(this).data('popup-open');
		popupOpen(popupName);
	});
}

function openCatalog() {
	$('.header__catalog').each(function(){
		$(this).on('click', function() {
			$(this).toggleClass('active');
			$(this).closest('.head').find('.catalogMenu').toggleClass('active');
		});
	});

}

function copyLink() {
	var input = $('#input-link');
	var btn = $('#copy-link');
	input.val(window.location.href);

	btn.on('click', function(){
		input.select();
		document.execCommand("copy");
	});
}

function showPassword() {
	var show = $('.show-pass');
	var hide = $('.hide-pass');

	show.each(function() {
		$(this).on('click', function() {
			$(this).siblings('input').attr('type', 'text');
			$(this).removeClass('active');
			hide.addClass('active');
		});
	});

	hide.on('click', function() {
		$(this).on('click', function() {
			$(this).siblings('input').attr('type', 'password');
			$(this).removeClass('active');
			show.addClass('active');
		});
	});
}

function orderLabels() {
	var label = $('.check-label');
	var input = $('.check-input');

	input.each(function(){
		$(this).on('change', function() {
			$(this).parent(label).find('.check-block').removeClass('opacity');
			$(this).parent(label).siblings().find('.check-block').addClass('opacity');
		});
	});
}

function selects() {
	var select = $('.custom-select');

	select.each(function() {
		$(this).niceSelect();
	});
}

function brandFilters() {
	let links = document.querySelectorAll('.sort-link');
	let items = document.querySelectorAll('.brandsPage__item');
	let letter = document.querySelectorAll('.brandsPage__item-letter');

	let filter = Array.prototype.filter;

	links.forEach(link => {

		link.addEventListener('click', function(event){
			event.preventDefault();
			let text = link.innerText;

			var x = links;
			Array.prototype.forEach.call(x, function(el) {
				el.classList.remove("active");
			})

			link.classList.add('active');

			filter.call(items, item => {
				if (text === item.getAttribute('data-letter')) {
					item.style.display = 'block';
				} else {
					item.style.display = 'none';
				}

				if (text === 'Все') {
					item.style.display = 'block';
				}
			})
		});

	})

}


function hideFixedHeaderLink() {
	let limit = 5;

	$(".fixed-header .nav-link:nth-child(n + " + (limit + 1) + ")").hide();

	$('.fixed-header .nav-link:hidden').clone().appendTo('.fixed-header .toggle-menu');

	$('.fixed-header .more-menu').on('click', function() {
		$('.fixed-header .toggle-menu .nav-link').show();
		$('.fixed-header .toggle-menu').slideToggle();
	});
}


function fixedHeader() {
	let header = $('.fixed-header');
	let container = $('.fixed-header-container');

	$('.header__events').clone().appendTo(container);
	$('.catalogMenu').clone().appendTo(header);
	let height = $('.header').height();
	let start = 0;
	let navOffsetTop = header.offset().top;
	$(window).on('scroll', function() {
		var thisTop = $(this).scrollTop();

		if (thisTop > height) {
			if (thisTop > start) {
				header.removeClass('active');
			} else {
				header.addClass('active');
			}
		} else {
			header.removeClass('active');
		}

		start = thisTop;
	});
}

function hideHeaderLink() {
	let limit = 5;

	$(".header .nav-link:nth-child(n + " + (limit + 1) + ")").hide();

	$('.header .nav-link:hidden').clone().appendTo('.header .toggle-menu');

	$('.header .more-menu').on('click', function() {
		$('.header .toggle-menu .nav-link').show();
		$('.header .toggle-menu').slideToggle();
	});
}


function mediaQuery() {
	$(window).on('load', function() {
		if ( $(window).width() <= 1024 ) {
			hideHeaderLink();
		}

		if ( $(window).width() <= 800) {
			$('.catalog__name').on('click', function(event) {
				$(this).toggleClass('active');
				$(this).next().slideToggle();
			});

			$('.main-slide img').each(function() {
				let mob = $(this).attr('data-mob');
				$(this).attr('src', mob);
			});
		}
	});
}


function replaceSymbols() {
	var numberInput = $('.number');
	numberInput.each(function() {
		$(this).bind('change keyup input click', function() {
			if (this.value.match(/[^0-9]/g)) {
				this.value = this.value.replace(/[^0-9]/g, '');
			}
		});
	});
}

function phoneMask() {
	$('.phone-mask').each(function() {
		$(this).mask("+7 (999) 999-99-99");
	});
}

function autocomplete() {
	var obj = ["Москва", "Новосибирск"];

	var input = $('#region');

	var region = '';

	input.keyup(function(e) {
		var val = $(this).val();

		if(val == '') {
			$('#complete').text('');
			return;
		}

		if (e.which === 37 || e.which === 39) {
			e.preventDefault();
			input.val(region);
			$('#complete').text('');
			return;
		}

		var find = false;
		for (var i = 0; i < obj.length; i++) {
			region = obj[i];
			if(region.indexOf(val) === 0) {
				find = true;
				break;
			} else {
				region = '';
			}
		}

		if(find === true) {
			$('#complete').text(region);
		} else {
			$('#complete').text('');
		}
	})
}
autocomplete();


function getCities() {
	var url = 'https://api.hh.ru/areas';
	var cities = [];
	var name = [];

	fetch(url)
	.then(res => res.json())
	.then(item => cities.push(item[0].areas))
	.catch(e => console.error(e))
}


function autoHeight() {
	let sw = $('.new__slider .swiper-slide');
	sw.each(function() {
		let  h = $(this).parent('.swiper-wrapper').height();
		$(this).css('height', h - 20);
	});
}